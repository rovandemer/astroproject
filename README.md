<a name="readme-top"></a>

<div align="center">
  <img src="doc/logo.png" alt="logo" width="140"  height="auto" />
  <br/>

<h3><b>Astro - A new Universe</b></h3>

</div>


# 📗 Table of Contents

- [📗 Table of Contents](#-table-of-contents)
- [📖 Astro ](#-astro-)
  - [🛠 Built With ](#-built-with-)
    - [Tech Stack ](#tech-stack-)
    - [Key Features ](#key-features-)
  - [💻 Getting Started ](#-getting-started-)
    - [Prerequisites](#prerequisites)
    - [Setup](#setup)
    - [Build](#build)
    - [Development](#development)
  - [🔭 Future Features ](#-future-features-)
  - [📝 License ](#-license-)

# 📖 Astro <a name="about-project"></a>


**Astro** is an Open-Source and free project to organize competitions and events.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

The project is built using the following technologies:

<details>
  <summary>Back-End</summary>
  <ul>
    <li><a href="https://go.dev">Go</a></li>
  </ul>
</details>

<details>
<summary>Front-End</summary>
  <ul>
    <li><a href="https://templ.guide">Templ</a></li>
    <li><a href="https://htmx.org">HTMX</a></li>
  </ul>
</details>

It is powered by the [Wails framework](http://wails.io).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Key Features <a name="key-features"></a>

> 👷‍ The project is currently under development.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need:

- [Go](https://golang.org/dl/)
- [WailsV3](https://v3alpha.wails.io/getting-started/installation/)

### Setup

Clone this repository to your desired folder:

```sh
  cd my-folder
  git clone git@gitlab.isima.fr:rovandemer/astroproject.git
```

### Build

Build the project using the following command:

```sh
  wails3 build
```

### Development

To run the project, execute the following command:

```sh
wails3 dev
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 🔭 Future Features <a name="future-features"></a>

- [ ] Add formulas to personalize the competitions
- [ ] Display the results of the competitions

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 📝 License <a name="license"></a>

This project is [GNU](LICENSE) licensed.

<p align="right">(<a href="#readme-top">back to top</a>)</p>